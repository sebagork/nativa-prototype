﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CCharacterController2D))]
public class CPlayer : MonoBehaviour
{
    // constants
    #region Animation triggers names
    string ANIM_SPEED = "Speed";
    string ANIM_BLEND = "Blend";
    string ANIM_ISGROUNDED = "IsGrounded";
    string ANIM_JUMP = "Jump";
    #endregion
    #region Input names
    string RUN_BUTTON = "Run";
    string JUMP_BUTTON = "Jump";
    #endregion

    //input info
    InputInfo _inputInfo;

    // reference to character controller 2D
    CCharacterController2D _characterController;

    // player jump height
    [SerializeField, Header("Velocity vars")]
    float _jumpHeight;

    // time to reach the max height
    [SerializeField]
    float _timeToReachMaxHeight;

    // to smooth the X velocity when grounded
    [SerializeField]
    float _accelerationTimeGrounded;

    // to smooth the X velocity when airborne
    [SerializeField]
    float _accelerationTimeAirborne;

    // player move speed
    float _moveSpeed;
    [SerializeField]
    float _walkSpeed, _runSpeed;

    // player jump velocity
    float _jumpVelocity;

    // player gravity
    float _gravity;

    // fall velocity limit
    [SerializeField]
    float _minYVelocity;

    // player velocity
    Vector3 _velocity;

    // to use with Mathf.SmoothDamp
    float _velocitySmoothing;    

    // character Animator
    [SerializeField, Header("3DModel")]
    Animator _animator;

    // 3DModel transform
    [SerializeField]
    Transform _characterTransform;

    // to anchor the 3DModel with the controller
    Transform _characterAnchor;

    // to improve the jump input feeling
    [SerializeField, Header("Better Jump")]
    float _jumpPressedRememberMax;
    float _jumpRememberTimer;
    [SerializeField]
    float _groundedRememberMax;
    float _groundedRememberTimer;
    
    // Player state machine
    enum State
    {        
        IDLE,
        WALKING,
        RUNNING,
        JUMPING,
        FALLING,
        HIT,
        DYING,
        WAITING,
    }

    [SerializeField, Header("Player State")]
    State _state;
    float _stateTime = 0;

    private void Start()
    {
        // setting up player
        _characterController = this.GetComponent<CCharacterController2D>();
        _characterAnchor = this.transform.GetChild(0).transform;
        SetState(State.IDLE);
        _groundedRememberTimer = _groundedRememberMax + 1;
        _jumpRememberTimer = _jumpPressedRememberMax + 1;

        // calculate gravity
        _gravity = -(2 * _jumpHeight) / Mathf.Pow(_timeToReachMaxHeight, 2);

        // calculate jump velocity needed to reach the jump
        _jumpVelocity = Mathf.Abs(_gravity) * _timeToReachMaxHeight;
    }

    private void Update()
    {
        #region Player Input
        // reset input info
        _inputInfo.Reset();

        // check move axis
        _inputInfo._moveAxis = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        // check run button
        if (Input.GetAxisRaw(RUN_BUTTON) == 1)
            _inputInfo._runButton = true;

        // check jump button
        if (Input.GetButtonDown(JUMP_BUTTON))
        {
            _inputInfo._jumpButton = true;
            _jumpRememberTimer = 0;
        }
        #endregion       

        // if there is collision above or below set Y vel to 0
        if (_characterController._collisionsInfo._above || _characterController._collisionsInfo._isGrounded)
        {
            _velocity.y = 0;
        }

        // to improve the jump 
        if (_jumpRememberTimer <= _jumpPressedRememberMax)
        {
            _jumpRememberTimer += Time.deltaTime;
        }                 

        // update corresponding to each state
        if (_state == State.IDLE)
        {
            if (Mathf.Abs(_inputInfo._moveAxis.x) > 0)
            {
                if (!_inputInfo._runButton)
                {
                    SetState(State.WALKING);
                }
                else
                {
                    SetState(State.RUNNING);
                }
            }   

            if (_jumpRememberTimer <= _jumpPressedRememberMax && _characterController._collisionsInfo._isGrounded)
                SetState(State.JUMPING);
        }
        else if (_state == State.WALKING)
        {
            if (Mathf.Abs(_inputInfo._moveAxis.x) == 0)
                SetState(State.IDLE);

            if (_inputInfo._runButton)
                SetState(State.RUNNING);

            if (_jumpRememberTimer <= _jumpPressedRememberMax && _characterController._collisionsInfo._isGrounded)
                SetState(State.JUMPING);

            if (!_characterController._collisionsInfo._isGrounded)
            {
                _groundedRememberTimer = 0;
                SetState(State.FALLING);
            }                
        }
        else if (_state == State.RUNNING)
        {
            if (Mathf.Abs(_inputInfo._moveAxis.x) == 0)
                SetState(State.IDLE);

            if (!_inputInfo._runButton)
                SetState(State.WALKING);

            if (_jumpRememberTimer <= _jumpPressedRememberMax && _characterController._collisionsInfo._isGrounded)
                SetState(State.JUMPING);

            if (!_characterController._collisionsInfo._isGrounded)
            {
                _groundedRememberTimer = 0;
                SetState(State.FALLING);
            }
        }
        else if (_state == State.JUMPING)
        {
            // start falling
            if (_characterController._collisionsInfo._oldVelocity.y <= 0)
                SetState(State.FALLING);
        }
        else if (_state == State.FALLING)
        {            
            // touch ground
            if (_characterController._collisionsInfo._isGrounded)
            {
                if (Mathf.Abs(_inputInfo._moveAxis.x) > 0)
                {
                    if (!_inputInfo._runButton)
                    {
                        SetState(State.WALKING);
                    }
                    else
                    {
                        SetState(State.RUNNING);
                    }
                }
                else
                {
                    SetState(State.IDLE);
                }
            }

            // to improve jump feeling
            _groundedRememberTimer += Time.deltaTime;
            if (_jumpRememberTimer <= _jumpPressedRememberMax && _groundedRememberTimer <= _groundedRememberMax)
            {
                _groundedRememberTimer += _groundedRememberMax;
                SetState(State.JUMPING);
            }                
        }

        // calculating x velocity target
        float tTargetVelocityX = _inputInfo._moveAxis.x * _moveSpeed;

        // applying x velocity with smooth
        _velocity.x = Mathf.SmoothDamp(_velocity.x, tTargetVelocityX, ref _velocitySmoothing, (_characterController._collisionsInfo._isGrounded) ? _accelerationTimeGrounded : _accelerationTimeAirborne);

        // apply Gravity 
        _velocity.y += _gravity * Time.deltaTime;

        // limit velocity y
        if (_velocity.y < _minYVelocity)
        {
            _velocity.y = _minYVelocity;
        }

        // move the controller
        _characterController.Move(_velocity * Time.deltaTime);

        // update 3DModel position and animate it
        _characterTransform.position = _characterAnchor.position;
        _animator.SetFloat(ANIM_SPEED, Mathf.Abs(_velocity.x));
        _animator.SetFloat(ANIM_BLEND, Mathf.Abs(_velocity.x) / _runSpeed);      
        _animator.SetBool(ANIM_ISGROUNDED, _characterController._collisionsInfo._isGrounded);

        // rotating the 3D model according to the X move direction
        if (_velocity.x != 0)
        {
            if (Mathf.Sign(_velocity.x) == 1)
            {
                _characterTransform.eulerAngles = new Vector3(0, 90, 0);
            }
            else
            {
                _characterTransform.eulerAngles = new Vector3(0, 270, 0);
            }
        }        
    }

    // to change the state of the player
    void SetState(State aState)
    {
        _state = aState;
        _stateTime = 0;

        if (_state == State.IDLE)
        {

        }
        else if (_state == State.WALKING)
        {
            _moveSpeed = _walkSpeed;
        }
        else if (_state == State.RUNNING)
        {
            _moveSpeed = _runSpeed;
        }
        else if (_state == State.JUMPING)
        {
            _velocity.y = _jumpVelocity;
            _animator.SetTrigger(ANIM_JUMP);
        }
        else if (_state == State.FALLING)
        {

        }
    }
    
    // info about the player inputs
    public struct InputInfo
    {
        public bool _runButton;
        public bool _jumpButton;
        public Vector2 _moveAxis;

        // reset buttons info
        public void Reset()
        {
            _runButton = false;
            _jumpButton = false;
        }
    }
}
