﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CTriggerAnimation : MonoBehaviour {

    // layers that activate the trigger
    [SerializeField]
    LayerMask _layers;

    // animations to be triggered
    [SerializeField]
    List<DOTweenAnimation> _animations;

    // paths to be triggered
    [SerializeField]
    List<DOTweenPath> _paths;

    private void OnTriggerEnter2D(Collider2D aCollision)
    {
        // check if the collision is correct
        if (_layers == (_layers | (1 << aCollision.gameObject.layer)))
        {
            // play animations
            if (_animations != null)
            {
                for (int i = 0; i < _animations.Count; i++)
                {
                    _animations[i].DOPlay();
                }
            }

            // start paths
            if (_paths != null)
            {
                for (int i = 0; i < _paths.Count; i++)
                {
                    _paths[i].DOPlay();
                }
            }

            // disable trigger
            gameObject.SetActive(false);
        }        
    }
}
